public class task {
    public static void main(String [] arg) {
        //Создание массива
        int [] mas= new int[10];
        for(int i=0;i<mas.length;i++){
            mas[i]=i*2;
        }
         //Цикл Do
        String t1="Цикл Do";
        System.out.println(t1);
        int z1=0;
        do{
            System.out.println(mas[z1]);
            z1++;
        }while(z1<mas.length);

        //Цикл While
        String t2="Цикл While";
        System.out.println(t2);
        int z2=0;
        while(z2<mas.length) {
            System.out.println(mas[z2]);
            z2++; }

        //Цикл For
        String t3="Цикл For";
        System.out.println(t3);
        for(int z3=0;z3<mas.length;z3++) {
            System.out.println(mas[z3]);
             }

        //Цикл For each
        String t4="Цикл For each";
        System.out.println(t4);
        for(int z4:mas) {
            System.out.println(z4);
        }

        //С последнего по первый элемент массива с помощью цикла for
        String t5="С последнего по первый элемент массива";
        System.out.println(t5);
        for(int z5=mas.length-1;z5>=0;z5--) {
            System.out.println(mas[z5]);
        }

        //Сумма элементов массива
        String t6="Сумма элементов массива=";
        int sum=0;
        for(int z6:mas) {
            sum+=z6;
        }
        System.out.println(t6+sum);

        //Сумма четных элементов массива
        String t7="Сумма четных элементов массива=";
        int sum1;
        int sum1a=0;
        for(int z7=0;z7<mas.length;z7++) {
            sum1=(mas[z7]%2==0)?mas[z7]:0;
            sum1a+=sum1;
        }
        System.out.println(t7+sum1a);

        //Сумма нечетных элементов массива
        String t8="Сумма нечетных элементов массива=";
        int sum2=0;
        for(int z8=0;z8<mas.length;z8++) {
            if (mas[z8]%2!=0){sum2+=mas[z8];
            }
        }
        System.out.println(t8+sum2);

        //Прямой и обратный парядок 2 циклами
        String t9="Прямой и обратный парядок 2 циклами";
        System.out.println(t9);
        for(int z9=0;z9<mas.length/2;z9++) {
            System.out.println(mas[z9]);
        }
        for(int z9a=mas.length-1;z9a>=mas.length/2;z9a--) {
            System.out.println(mas[z9a]);
        }

        //Прямой и обратный парядок 1 циклом
        String t10="Прямой и обратный парядок 1 циклом";
        System.out.println(t10);
        int c=mas.length-1;
        for(int z10=0;z10<mas.length;z10++) {
            if(z10<mas.length/2) {
                System.out.println(mas[z10]);
            }else{
                System.out.println(mas[c--]);
                            }
        }
    }
}
